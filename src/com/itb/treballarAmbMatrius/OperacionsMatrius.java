package com.itb.treballarAmbMatrius;
import java.util.Scanner;

/**
 * @see OperacionsMatrius
 * Esta es la clase donde se ecuentran todas las operaciones para trabajar con matrices.
 */
public class OperacionsMatrius {
    /**
     * Es un metodo de relleno con numeros aleatorios una matriz.
     * @param matrix Ubica la matriz a rellenar
     * @param max El valor maximo de los números dentro de la matriz.
     * @param min El valor mínimo de los números dentro de la matriz.
     */
    public static void fillRandomMatrix(int[][] matrix, int max, int min){
        for (int i=0; i<matrix.length;i++){
            for(int j=0; j<matrix[0].length;j++) {
                matrix[i][j] = (int) Math.floor(Math.random() * (max - min + 1) + min);
            }
        }
    }

    /**
     * Ubica los valores dados por el usuario en el array.
     * @param matri es la matriz a la que se le insertaran los datos.
     */
    public static void setMatrixUserValues(int[][] matri){
        int counter=matri.length*matri[0].length;
        for (int i=0; i<matri.length;i++){
            for(int j=0; j<matri[0].length;j++) {
                matri[i][j] = getNumberUser();
                System.out.println(--counter+" numbers left...");
            }
        }
    }

    /**
     * @return El metodo devuelve un valor entero dado por el usuario.
     */
    private static int getNumberUser(){
        Scanner reader=new Scanner(System.in);
        System.out.print("Please insert the values of the matrix: ");
        while(!reader.hasNextInt()){
            if(!reader.hasNextInt()){
                System.out.print("That's not a whole number.\nTry again: ");
                reader.nextLine();
            }
        }
        System.out.println("loading...\nValue saved Succesfully!");
        return reader.nextInt();
    }

    /**
     * @param matrix es la matriz de la cual se guardaran los datos.
     * @return devuelve un auxiliar con los valores del array.
     */
    public static int[][] savedArrayValues(int[][] matrix){
        int[][] aux=new int[matrix.length][matrix[0].length];
        for(int i=0; i<aux.length;i++){
            System.arraycopy(matrix[i], 0, aux[i], 0, aux[0].length);

        }
        return aux;
    }

    /**
     * Es el proceso de adición de 2 matrices.
     * @param matrix1 es la variable de la primera matriz.
     * @param matrix2 es la variable de la segunda matriz.
     */
    public static int[][] matrixesAddition(int[][] matrix1, int[][] matrix2) {
        if(matrixesCanOperate(matrix1,matrix2)) {
            int[][] matrixResult = new int[matrix1.length][matrix1[0].length];
            for(int i=0; i<matrix1.length;i++){
                for(int j=0; j<matrix1[0].length;j++){
                    matrixResult[i][j]=matrix1[i][j]+matrix2[i][j];
                }
            }
            return matrixResult;
        }
        System.out.println("These 2 matrixes can't Add to each other.");
        return new int[0][0];
    }

    /**
     * Es el proceso de sustracción de 2 matrices.
     * @param matrix1 es la variable de la primera matriz.
     * @param matrix2 es la variable de la segunda matriz.
     */
    public static int[][] matrixesSustraction(int[][] matrix1, int[][] matrix2) {
        if(matrixesCanOperate(matrix1,matrix2)) {
            int[][] matrixResult = new int[matrix1.length][matrix1[0].length];
            for(int i=0; i<matrix1.length;i++){
                for(int j=0; j<matrix1[0].length;j++){
                    matrixResult[i][j]=matrix1[i][j]-matrix2[i][j];
                }
            }
            return matrixResult;
        }
        System.out.println("These 2 matrixes can't Sustract each other.");
        return new int[0][0];
    }

    /**
     * Proceso de multiplicación de 2 matrices.
     * @param matrix1 es la variable de la primera matriz.
     * @param matrix2 es la variable de la segunda matriz.
     */
    public static int[][] matrixesProduct(int[][] matrix1, int[][] matrix2){
        if(matrixesCanMultiply(matrix1,matrix2)) {
            int[][] matrixResult = new int[matrix1.length][matrix2[0].length];
            for(int i=0; i<matrix1.length;i++){
                for(int j=0; j<matrix2[0].length;j++){
                    for(int k=0; k<matrix2.length;k++) {
                        matrixResult[i][j] += matrix1[i][k] * matrix2[k][j];
                    }
                }
            }
            return matrixResult;
        }
        System.out.println("These 2 matrixes can't multiply each other.");
        return new int[0][0];
    }

    /**
     * Verifica que pueden multiplicarse 2 matrices.
     * @param matrix1 es la variable de la primera matriz.
     * @param matrix2 es la variable de la segunda matriz.
     * @return devuelve un boolean(true/false) que dice si es posible o no posible.
     */
    public static boolean matrixesCanMultiply(int[][] matrix1, int[][] matrix2) {
        return matrix1[0].length==matrix2.length;
    }

    /**
     * Verifica si las matrices pueden hacer sustracción y/o adición entre ellas.
     * @param matrix1 es la variable de la primera matriz.
     * @param matrix2 es la variable de la segunda matriz.
     * @return devuelve un boolean(true/false) que dice si es posible o no posible.
     */
    public static boolean matrixesCanOperate(int[][] matrix1, int[][] matrix2){
        return matrix1.length==matrix2.length && matrix1[0].length == matrix2[0].length;
    }

    /**
     * Verifica si una matriz es cuadrada.
     * @param matrix parametro de la matriz a verificar.
     * @return devuelve un boolean(true/false) para decir que es o no es cuadrada.
     */
    public static boolean squareMatrix(int[][] matrix){
        return matrix.length==matrix[0].length;
    }
}
