import com.itb.matrius.Matriu;
import com.itb.treballarAmbMatrius.OperacionsMatrius;

import java.util.Scanner;

/**
 * @author Ricardo Montserrat Solorzano
 * @version 1.0
 * @see ProvaTreballarAmbMatrius
 * Esto es la clase principal por defecto desde donde se iniciara el programa de prueba.
 */
public class ProvaTreballarAmbMatrius {
    public static void main(String[] args) {
        ProvaTreballarAmbMatrius program = new ProvaTreballarAmbMatrius();
        Matriu matrix1 = new Matriu(3, 2);
        Matriu matrix2 = new Matriu(2, 3);
        program.startTrial(matrix1, matrix2);
    }

    /**
     * Programa principal
     *
     * @param mx  matriz primera a utilizar.
     * @param mx2 matriz segunda a utilizar.
     */
    private void startTrial(Matriu mx, Matriu mx2) {
        /*tryGetsAndSets(mx);
        pause();*/
        tryOperations(mx, mx2);
        pause();
    }

    /**
     * Metodo de pausa entre procesos para darle un espacio de visualización al usuario.
     */
    private void pause() {
        Scanner reader = new Scanner(System.in);
        System.out.print("Press the key enter to continue...");
        reader.nextLine();
    }

    /**
     * El metodo prueba los set y gets creados para objetos matrices.
     *
     * @param matrix es el objeto matriz a utilizar.
     */
    private void tryGetsAndSets(Matriu matrix) {
        System.out.println("The number of rows is " + matrix.getRows());
        System.out.println("The number of columns is " + matrix.getColumns() + "\n");

        OperacionsMatrius.fillRandomMatrix(matrix.getMatrix(), 20, 0);
        System.out.println("The matrix is:\n" + matrix.toString());

        int[][] aux = OperacionsMatrius.savedArrayValues(matrix.getMatrix());
        matrix.setColumns(3);
        matrix.setRows(3);
        matrix.setMatrixValues(aux);
        System.out.println("The new matrix is:\n" + matrix.toString());
    }

    /**
     * Metodo de verificación de operaciones simples entre matrices.
     *
     * @param matrix1 matriz primera variable.
     * @param matrix2 matriz segunda variable.
     */
    private void tryOperations(Matriu matrix1, Matriu matrix2) {
        System.out.println("The first matrix being square is " + OperacionsMatrius.squareMatrix(matrix1.getMatrix()));
        System.out.println("The second matrix being square is " + OperacionsMatrius.squareMatrix(matrix2.getMatrix()) + "\n");

        OperacionsMatrius.setMatrixUserValues(matrix1.getMatrix());
        System.out.println("The matrix is:\n" + matrix1.toString());
        OperacionsMatrius.setMatrixUserValues(matrix2.getMatrix());
        System.out.println("The matrix is:\n" + matrix2.toString());

        Matriu resultMatrix = new Matriu(0, 0);
        if (OperacionsMatrius.matrixesCanOperate(matrix1.getMatrix(), matrix2.getMatrix())) {
            resultMatrix.setMatrix(OperacionsMatrius.matrixesAddition(matrix1.getMatrix(), matrix2.getMatrix()));
            System.out.println("The result of the addition is:\n" + resultMatrix.toString());
            resultMatrix.setMatrix(OperacionsMatrius.matrixesSustraction(matrix1.getMatrix(), matrix2.getMatrix()));
            System.out.println("The result of the sustraction is:\n" + resultMatrix.toString());
        }
        if (OperacionsMatrius.matrixesCanMultiply(matrix1.getMatrix(), matrix2.getMatrix())) {
            resultMatrix.setMatrix(OperacionsMatrius.matrixesProduct(matrix1.getMatrix(), matrix2.getMatrix()));
            System.out.println("The result of the product is:\n" + resultMatrix.toString());
        }
    }
}
