package com.itb.matrius;

import org.w3c.dom.ls.LSOutput;

/**
 * Esta es la clase de la Matriz, la cual trabaja con columnas y filas.
 */
public class Matriu {
    /**
     * Representa las filas.
     * Representa las Columnas.
     * Representa la matriz en base a las filas y columnas.
     */
    private int rows;
    private int columns;
    private int[][] matrix;

    /**
     * Metodo constructor.
     * @param ro  representa el valor de las columnas
     * @param col representa el valaor de las filas
     */
    public Matriu(int ro, int col) {
        rows = ro;
        columns = col;
        matrix = new int[rows][columns];
    }

    /**
     * @param row Ubica el valor de la fila de la matriz.
     */
    public void setRows(int row) {
        rows = row;
        matrix = new int[row][columns];
    }

    /**
     * @param column Ubica el valor de la columna de la matriz.
     */
    public void setColumns(int column) {
        columns = column;
        matrix = new int[rows][column];
    }

    /**
     * @param aux
     * Otorga los valores del auxiliar a la matriz.
     */
    public void setMatrixValues(int[][] aux) {
        for (int i = 0; i < aux.length && i<matrix.length; i++) {
            for (int j = 0; j < aux[0].length && j<matrix[0].length; j++) {
                matrix[i][j] = aux[i][j];
            }
        }
    }

    /**
     * Cambio de matriz principal.
     * @param matrix es la matriz a tomar lugar como principal.
     */
    public void setMatrix(int[][] matrix){
        this.matrix=matrix;
        rows=matrix.length;
        columns=matrix[0].length;
    }
    /**
     * @return Devuelve el valor de la fila de la matriz.
     */
    public int getRows() {
        return rows;
    }

    /**
     * @return Devuelve el valor de la columna de la matriz.
     */
    public int getColumns() {
        return columns;
    }

    /**
     * @return Devuelve el valor de la matriz.
     */
    public int[][] getMatrix() {
        return matrix;
    }

    /**
     * @return Devuelve en forma de string a la matriz.
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int[] ints : matrix) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (ints[j] < 10 && ints[j] > -1) {
                    result.append("0");
                }
                result.append(ints[j]);
                result.append(" ");
            }
            result.append("\n");
        }
        return result.toString();
    }
}
